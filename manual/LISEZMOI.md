![modderbrothers](../assets/product.png)

# Manuel - Mod RGB Intellivision

[English Version Here](./README.md)

## Outillage requit

Obligatoire pour l'installation du mod :
- Tournevis cruciforme
- Fer à souder + étain
- Perceuse + foret de 15 ou scie cloche de 15 - scie cloche recommandée

## Installation du Mod

#### Précautions générales

- Toutes les manipulations doivent être effectuées hors tension.
- Vous êtes responsables de votre outillage et de ses manipulations. Si vous n'avez pas les aptitudes et/ou l'expérience requise, nous vous conseillons de faire réaliser ces manipulations par une personne qualifiée.

#### Précautions à prendre avant de toucher les cartes et les composants électroniques

Certains composants sont très sensibles à l'électricité statique.
Veillez à vous décharger avant l'ouverture de votre ZX-81 :
- Placez-vous dans une piece en carrelage, et évitez la moquette !
- Évitez les pulls en laine ou autres vêtements qui se chargent facilement !
- Touchez la carcasse métallique d'un appareil électrique pour vous décharger à la terre.

#### Ouverture de la console
