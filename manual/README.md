![modderbrothers](../assets/product.png)

# Manual - Intellivision RGB Mod

[Version Française ici](./LISEZMOI.md)

## Tools required

Required for the installation of the mod :
- Screwdriver
- Soldering iron + tin
- Drill + 15mm drill bit or 15mm hole saw - hole saw recommended

## Mod Installation

#### General precautions

- All manipulations must be carried out without power.
- You are responsible for your tools and their handling. If you do not have the required skills and/or experience, we advise you to have these manipulations done by a qualified person.

#### Precautions to take before touching the boards and electronic components

Some components are very sensitive to electrostatic discharge.
Be sure to discharge yourself before opening your VG5000:
- Place yourself in a tiled room, and avoid carpeting!
- Avoid wool sweaters or other clothes that charge easily!
- Touch the metal casing of an electrical appliance to discharge to ground.

#### Opening your ZX-81
